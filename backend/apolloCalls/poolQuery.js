export const poolQuery = `
query($id: ID!, $block: Block_height) { 
 pool(id: $id, block: $block) {
   tick
     token0 {
         symbol
         id
         decimals
       }
     token1 {
         symbol
         id
         decimals
       }
     feeTier
     feesUSD
     volumeUSD
     totalValueLockedUSD
     }
 }  
`;
