export const getFragolaTvlOfEachToken = async (
  FragolaContract,
  block,
  token0,
  token1
) => {
  const fragolaUserAmounts = await FragolaContract.methods.usersAmounts
    .call()
    .call(null, block);
  const TVLToken0 =
    (parseInt(fragolaUserAmounts.amount0) /
      Math.pow(10, parseInt(token0.decimals))) *
    token0.token0Price;
  const TVLToken1 =
    (parseInt(fragolaUserAmounts.amount1) /
      Math.pow(10, parseInt(token1.decimals))) *
    token1.token1Price;
  return {
    TVLToken0,
    TVLToken1,
  };
};

export const getFragolaTotalFeesOfEachToken = async (
  FragolaContract,
  block,
  token0,
  token1
) => {
  const fragolaTotalFees0 = await FragolaContract.methods.totalFees0
    .call()
    .call(null, block);
  const fragolaTotalFees1 = await FragolaContract.methods.totalFees1
    .call()
    .call(null, block);

  const feesToken0 =
    (parseInt(fragolaTotalFees0) / Math.pow(10, parseInt(token0.decimals))) *
    token0.token0Price;

  const feesToken1 =
    (parseInt(fragolaTotalFees1) / Math.pow(10, parseInt(token1.decimals))) *
    token1.token1Price;

  return {
    feesToken0,
    feesToken1,
  };
};
