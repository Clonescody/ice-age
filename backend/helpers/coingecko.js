import fetch from "node-fetch";
import config from "../config.js";

export const getPriceOfEachToken = async (idToken0, idToken1) => {
  const responseToken0Gecko = await fetch(
    `${config.coingeckoApiURL}/coins/${idToken0}`
  );
  const datasToken0Gecko = await responseToken0Gecko.json();
  const token0Price = datasToken0Gecko.market_data.current_price.usd;

  const responseToken1Gecko = await fetch(
    `${config.coingeckoApiURL}/coins/${idToken1}`
  );
  const datasToken1Gecko = await responseToken1Gecko.json();
  const token1Price = datasToken1Gecko.market_data.current_price.usd;
  return {
    token0Price,
    token1Price,
  };
};
