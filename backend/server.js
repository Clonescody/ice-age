import {
  ApolloClient,
  InMemoryCache,
  gql,
} from "@apollo/client/core/core.cjs.js";
import { createHttpLink } from "apollo-link-http";
import { createRequire } from "module";
import express from "express";
import cors from "cors";
import CacheService from "express-api-cache";
import timeout from "connect-timeout";
import fetch from "node-fetch";
import Web3 from "web3";
import EthDater from "ethereum-block-by-date";
import moment from "moment";
import config from "./config.js";
import { poolQuery } from "./apolloCalls/poolQuery.js";
import { getPriceOfEachToken } from "./helpers/coingecko.js";
import {
  getFragolaTotalFeesOfEachToken,
  getFragolaTvlOfEachToken,
} from "./helpers/fragola.js";

const PORT = 5000;
const app = express();
const cache = CacheService.cache;

app.use(cors());
app.use(timeout(600000));
app.use(express.json());

const corsOptions = {
  origin:
    config.env === "dev"
      ? "http://localhost:3001"
      : "https://www.ice-age.money",
};

const API_URL = "https://api.thegraph.com/subgraphs/name/uniswap/uniswap-v3";
const RPCUrl = `${config.infuraURL}/${config.infuraKey}`;

const web3 = new Web3(RPCUrl);
const dater = new EthDater(web3);

const apolloClient = new ApolloClient({
  link: createHttpLink({
    uri: API_URL,
    fetch: fetch,
  }),
  cache: new InMemoryCache(),
});

const require = createRequire(import.meta.url);
const fragolaPools = require("./datas/fragolaPools.json");
const fragolaABI = require("./ABI/fragola_abi.json");

app.get("/getPoolsList", cache("30 seconds"), async (req, res) => {
  const pools = fragolaPools.pools.map((pool) => {
    return {
      name: pool.name,
      address: pool.address,
      logo: pool.logo,
      chain: pool.chain,
    };
  });
  res.json(pools);
});

app.post("/getPoolInfos", cache("30 seconds"), async (req, res) => {
  const fragolaPool = fragolaPools.pools.find(
    (element) => element.address === req.body.address
  );
  const { pool, address, logo, chain, token0, token1, fee } = fragolaPool;
  const FragolaContract = new web3.eth.Contract(fragolaABI, address);

  const block = await dater.getDate(moment(), false);
  const lastWeekBlock = await dater.getDate(moment().subtract(1, "w"), false);

  // Local infos of the pool

  let finalPoolObject = {
    uniPoolAddress: pool.toLowerCase(),
    uniFeePercentage: `${fee}`,
    fragolaAddress: address.toLowerCase(),
    fragolaLogo: logo,
    chain,
    token0,
    token1,
  };

  const lastWeekPoolInfos = await apolloClient.query({
    query: gql(poolQuery),
    variables: {
      id: finalPoolObject.uniPoolAddress,
      block: { number: lastWeekBlock.block - 10 },
    },
  });
  const uniFeesLastWeek = lastWeekPoolInfos.data.pool.feesUSD;

  const currentPoolInfos = await apolloClient.query({
    query: gql(poolQuery),
    variables: {
      id: finalPoolObject.uniPoolAddress,
      block: { number: 13605882 - 10 },
    },
  });

  // Uniswap pool infos from TheGraph

  finalPoolObject = {
    ...finalPoolObject,
    token0: {
      ...token0,
      ...currentPoolInfos.data.pool.token0,
      id: token0.id,
    },
    token1: {
      ...token1,
      ...currentPoolInfos.data.pool.token1,
      id: token1.id,
    },
  };

  // Coingecko price

  const pairPrices = await getPriceOfEachToken(
    finalPoolObject.token0.id,
    finalPoolObject.token1.id
  );

  // Fragola TVL

  const fragolaPairTVL = await getFragolaTvlOfEachToken(
    FragolaContract,
    block.block,
    {
      token0Price: pairPrices.token0Price,
      decimals: finalPoolObject.token0.decimals,
    },
    {
      token1Price: pairPrices.token1Price,
      decimals: finalPoolObject.token1.decimals,
    }
  );

  // Fragola fees

  const fragolaPairTotalFees = await getFragolaTotalFeesOfEachToken(
    FragolaContract,
    block.block,
    {
      token0Price: pairPrices.token0Price,
      decimals: finalPoolObject.token0.decimals,
    },
    {
      token1Price: pairPrices.token1Price,
      decimals: finalPoolObject.token1.decimals,
    }
  );

  finalPoolObject = {
    ...finalPoolObject,
    fragolaName: `${currentPoolInfos.data.pool.token0.symbol}/${currentPoolInfos.data.pool.token1.symbol} ${finalPoolObject.uniFeePercentage}%`,
    fragolaFees: `${
      fragolaPairTotalFees.feesToken0 + fragolaPairTotalFees.feesToken1
    }`,
    fragolaTVL: `${fragolaPairTVL.TVLToken0 + fragolaPairTVL.TVLToken1}`,
    uniVolumeUSD: currentPoolInfos.data.pool.volumeUSD,
    uniTVL: currentPoolInfos.data.pool.totalValueLockedUSD,
    uniFeesUSD: `${
      parseFloat(currentPoolInfos.data.pool.feesUSD) -
      parseFloat(uniFeesLastWeek)
    }`,
    token0: {
      ...finalPoolObject.token0,
      price: `${pairPrices.token0Price}`,
    },
    token1: {
      ...finalPoolObject.token1,
      price: `${pairPrices.token1Price}`,
    },
  };
  console.log("final : ", finalPoolObject);
  res.json(finalPoolObject);
});

app.listen(PORT, () => {
  console.log(`Server listening for ${corsOptions.origin} requests ...`);
});
