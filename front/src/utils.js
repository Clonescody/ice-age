export const getChartWidthForSize = (width) => {
  if (width >= 610) {
    return "600px";
  }
  if (width < 610 && width > 510) {
    return "500px";
  }
  if (width < 510 && width >= 390) {
    return "400px";
  }
  if (width < 390 && width > 320) {
    return "330px";
  }
  return "300px";
};
