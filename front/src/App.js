import "./styles/App.css";
import Routing from "./components/Navigation/Routing";
import Header from "./components/Navigation/Header";
import Footer from "./components/Navigation/Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <div className="App-container">
        <Routing />
      </div>
      <Footer />
    </div>
  );
}

export default App;
