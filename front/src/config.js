/* eslint-disable no-undef */
export default {
  backend:
    process.env.NODE_ENV === "production"
      ? "https://www.ice-age.money/api"
      : "http://localhost:5000",
};
