import { Routes, Route, Outlet } from "react-router-dom";
import IcebergScreen from "../../screens/IcebergScreen";
import PoolsListScreen from "../../screens/PoolsListScreen";
import PoolDetailsScreen from "../../screens/PoolDetailsScreen";
import NotFound from "../../screens/NotFound";
import StakingScreen from "../../screens/StakingScreen";

const Routing = () => {
  return (
    <Routes>
      <Route path="/" element={<IcebergScreen />} />
      <Route
        path="pools"
        element={
          <>
            <Outlet />
          </>
        }
      >
        <Route path="/pools/" element={<PoolsListScreen />} />
        <Route path=":address" element={<PoolDetailsScreen />} />
      </Route>
      <Route path="staking" element={<StakingScreen />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
};

export default Routing;
