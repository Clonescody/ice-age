import "../../styles/Navigation/Header.css";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <nav className="App-header">
      <Link className="App-header-link" to="/">
        Home
      </Link>
      <Link className="App-header-link" to="pools">
        Pools
      </Link>
      <Link className="App-header-link" to="staking">
        Staking
      </Link>
    </nav>
  );
};

export default Header;
