import "../../styles/Navigation/Footer.css";

const Footer = () => (
  <footer className="App-footer">
    <div className="App-links-container">
      <p>
        {"Built with french magic by "}
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/MelenXYZ"
          rel="noreferrer"
        >
          Mélen
        </a>
        {", "}
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/DefiNeuro"
          rel="noreferrer"
        >
          Neuro
        </a>
        {" & "}
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/Clonescody"
          rel="noreferrer"
        >
          Clonescody
        </a>
      </p>
    </div>
  </footer>
);

export default Footer;
