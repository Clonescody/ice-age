import "../styles/Pools/PoolsList.css";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import MyLoader from "../components/MyLoader";
import config from "../config";

const PoolsListScreen = () => {
  const [poolsList, setPoolsList] = useState([]);
  const [poolsListLoaded, setPoolsListLoaded] = useState(false);

  const fetchPools = async () => {
    const responsePools = await fetch(`${config.backend}/getPoolsList`);
    const poolsJson = await responsePools.json();
    setPoolsList(poolsJson);
    setPoolsListLoaded(true);
  };

  useEffect(() => {
    fetchPools();
  }, []);

  if (!poolsListLoaded) {
    return (
      <div className="PoolsList-loader-container">
        <MyLoader />
      </div>
    );
  }

  return (
    <div className="PoolsList-container">
      <h2>Pools available on Fragola</h2>
      <div className="PoolsList-list">
        {poolsList.map(({ logo, address, name }) => {
          const assets = name.split(" ")[0];
          const feesRange = name.split(" ")[1];
          return (
            <div className="PoolsList-Tile-container" key={address}>
              <img src={logo} width={50} height$={50} />
              <div>
                <p className="PoolsList-Tile-name">{assets}</p>
                <p className="PoolsList-Tile-name">{feesRange}</p>
              </div>
              <Link className="PoolsList-Tile-link" to={`/pools/${address}`}>
                View details
              </Link>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default PoolsListScreen;
