import "../styles/Pools/PoolDetails.css";
import { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import NumberFormat from "react-number-format";
import { useParams } from "react-router";

import MyLoader from "../components/MyLoader";
import config from "../config";
import colors from "../styles/colors";
import { getChartWidthForSize } from "../utils";

const PoolDetailsScreen = () => {
  const { address } = useParams();
  const [loadedInfos, setLoadedInfos] = useState(false);
  const [poolInfos, setPoolInfos] = useState({});

  let resizeCallToClear = null;
  const [width, setWidth] = useState(window.innerWidth);
  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(setWidth(window.innerWidth), 100);
    }
    window.addEventListener("resize", handleResize);
  }, []);

  const fetchPoolInfos = async () => {
    const response = await fetch(`${config.backend}/getPoolInfos`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ address }),
    });
    const infos = await response.json();
    setPoolInfos(infos);
    setLoadedInfos(true);
  };

  useEffect(() => {
    fetchPoolInfos();
  }, []);

  if (!loadedInfos) {
    return (
      <div className="PoolDetails-loader-container">
        <MyLoader />
      </div>
    );
  }

  const TVLOptions = {
    stroke: {
      show: true,
      curve: "smooth",
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      type: "datetime",
      categories: [
        "2018-09-19T00:00:00.000Z",
        "2018-09-19T01:30:00.000Z",
        "2018-09-19T02:30:00.000Z",
        "2018-09-19T03:30:00.000Z",
        "2018-09-19T04:30:00.000Z",
        "2018-09-19T05:30:00.000Z",
        "2018-09-19T06:30:00.000Z",
      ],
      labels: {
        style: {
          colors: "white",
        },
      },
    },
    yaxis: {
      labels: {
        style: {
          colors: "white",
        },
      },
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm",
      },
      enabled: true,
      theme: "dark",
    },
    legend: {
      show: true,
      labels: {
        colors: ["white"],
      },
    },
  };

  const options = {
    legend: {
      show: true,
      position: "bottom",
      labels: {
        colors: ["white"],
      },
      fontSize: width < 650 ? "12px" : "17px",
      horizontalAlign: "center",
      inverseOrder: true,
    },
    tooltip: {
      fillSeriesColor: true,
      style: {
        fontSize: "18px",
      },
    },
    colors: [colors.primary, colors.uniswap],
  };

  const TLVSeries = [
    {
      name: "series1",
      data: [31, 40, 28, 51, 42, 109, 100],
    },
    {
      name: "series2",
      data: [11, 32, 45, 32, 34, 52, 41],
    },
  ];

  const {
    fragolaLogo,
    fragolaAddress,
    uniPoolAddress,
    fragolaName,
    fragolaTVL,
    uniTVL,
    fragolaFees,
    uniFeesUSD,
    uniFeePercentage,
  } = poolInfos;

  return (
    <div className="PoolDetails-container">
      <div className="PoolDetails-title-container">
        <img
          src={fragolaLogo}
          height={50}
          width={50}
          className="PoolDetails-logo"
        />
        <h2 className="PoolDetails-title">{fragolaName}</h2>
      </div>
      <div className="PoolDetails-separator" />
      <div className="PoolDetails-Graphs-container">
        <div className="PoolDetails-Tvl-container">
          <h3 className="PoolDetails-Tvl-title">Historical TVL distribution</h3>
          <Chart
            options={TVLOptions}
            series={TLVSeries}
            type="area"
            width={getChartWidthForSize(width)}
          />
        </div>
        <div className="PoolDetails-Pies-container">
          <h3>Current pool TVL</h3>
          <NumberFormat
            prefix="Total : "
            suffix="$"
            value={parseInt(uniTVL)}
            displayType="text"
            thousandSeparator
            renderText={(value) => (
              <p className="PoolDetails-informations-value">{value}</p>
            )}
          />
          <Chart
            options={{
              ...options,
              labels: ["Fragola TVL", "Other LPs TVL"],
            }}
            series={[
              parseFloat(fragolaTVL),
              parseFloat(uniTVL) - parseFloat(fragolaTVL),
            ]}
            type="donut"
            width="350"
          />
          <h3>Current fees capture</h3>
          <Chart
            options={{ ...options, labels: ["Fragola fees", "Uniswap fees"] }}
            series={[parseFloat(fragolaFees), parseFloat(uniFeesUSD)]}
            type="donut"
            width="350"
          />
        </div>
      </div>
      <div className="PoolDetails-separator" />
      <h3 className="PoolDetails-informations-title">Informations</h3>
      <div className="PoolDetails-informations-links">
        <p>{`LP fees tier : ${uniFeePercentage}%`}</p>
        <p>
          {"Fragola address : "}
          <a
            className="App-link"
            target="_blank"
            href={`https://etherscan.io/address/${fragolaAddress}`}
            rel="noreferrer"
          >
            {`${fragolaAddress}`}
          </a>
        </p>
        <p>
          {"Uniswap pool address : "}
          <a
            className="App-link"
            target="_blank"
            href={`https://etherscan.io/address/${uniPoolAddress}`}
            rel="noreferrer"
          >
            {`${uniPoolAddress}`}
          </a>
        </p>
      </div>
      <div className="PoolDetails-informations-container">
        <h4>Uniswap</h4>
        <div className="PoolDetails-separator" />
        <div className="PoolDetails-informations-row">
          <div className="PoolDetails-informations-details">
            <h5>Pool TVL on Uniswap</h5>
            <NumberFormat
              prefix="$"
              value={parseInt(uniTVL)}
              displayType="text"
              thousandSeparator
              renderText={(value) => (
                <p className="PoolDetails-informations-value">{value}</p>
              )}
            />
          </div>
          <div className="PoolDetails-informations-details">
            <h5>Total fees on Uniswap</h5>
            <NumberFormat
              prefix="$"
              value={parseInt(uniFeesUSD)}
              displayType="text"
              thousandSeparator
              renderText={(value) => (
                <p className="PoolDetails-informations-value">{value}</p>
              )}
            />
          </div>
        </div>
        <h4>Fragola</h4>
        <div className="PoolDetails-separator" />
        <div className="PoolDetails-informations-row">
          <div className="PoolDetails-informations-details">
            <h5>TVL managed by Fragola</h5>
            <NumberFormat
              prefix="$"
              value={parseInt(fragolaTVL)}
              displayType="text"
              thousandSeparator
              renderText={(value) => (
                <p className="PoolDetails-informations-value">{value}</p>
              )}
            />
          </div>
          <div className="PoolDetails-informations-details">
            <h5>Total fees earned</h5>
            <NumberFormat
              prefix="$"
              value={parseInt(fragolaFees)}
              displayType="text"
              thousandSeparator
              renderText={(value) => (
                <p className="PoolDetails-informations-value">{value}</p>
              )}
            />
            <h5>LP fees earned (80%)</h5>
            <NumberFormat
              prefix="$"
              value={parseInt(0.8 * fragolaFees)}
              displayType="text"
              thousandSeparator
              renderText={(value) => (
                <p className="PoolDetails-informations-value">{value}</p>
              )}
            />
            <h5>Protocol fees (20%)</h5>
            <NumberFormat
              prefix="$"
              value={parseInt(0.2 * fragolaFees)}
              displayType="text"
              thousandSeparator
              renderText={(value) => (
                <p className="PoolDetails-informations-value">{value}</p>
              )}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PoolDetailsScreen;
